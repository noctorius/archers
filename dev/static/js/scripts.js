// Инициализация Vue.js
const MOBILE_BREAKPOINT = 1024;
const MobileMixin =  {
    data: () => ({
        isMobile: false,
    }),

    beforeDestroy () {
        if (typeof window !== 'undefined') {
            window.removeEventListener('resize', this.onResize, { passive: true })
        }
    },

    mounted () {
        this.onResize()
        window.addEventListener('resize', this.onResize, { passive: true })
    },

    methods: {
        onResize () {
            this.isMobile = window.innerWidth < MOBILE_BREAKPOINT
        },
    },
};

const dataEmbassys = [
    {
        title: "UAE Embassy",
        subtitle:"5 Day Turnaround*",
        description: "All UK documents accepted Personal Papers Business Documents",
        country: 'uae',
        image: '/static/images/svg/uae.svg'
    },
    {
        title: "Kuwait Embassy",
        subtitle:"5 Day Turnaround*",
        description: "All UK documents accepted Personal Papers Business Documents",
        country: 'kuwait',
        image: '/static/images/svg/kuwait.svg'
    },
    {
        title: "Jordan Embassy",
        subtitle:"5 Day Turnaround*",
        description: "All UK documents accepted Personal Papers Business Documents",
        country: 'jordan',
        image: '/static/images/svg/jordan.svg'
    },
    {
        title: "China Embassy",
        subtitle:"5 Day Turnaround*",
        description: "All UK documents accepted Personal Papers Business Documents",
        country: 'china',
        image: '/static/images/svg/china.svg'
    },
    {
        title: "Egypt Embassy",
        subtitle:"5 Day Turnaround*",
        description: "All UK documents accepted Personal Papers Business Documents",
        country: 'egypt',
        image: '/static/images/svg/egypt.svg'
    },
    {
        title: "Vietnam Embassy",
        subtitle:"5 Day Turnaround*",
        description: "All UK documents accepted Personal Papers Business Documents",
        country: 'vietnam',
        image: '/static/images/svg/vietnam.svg'
    },
    {
        title: "Philippines Embassy",
        subtitle:"5 Day Turnaround*",
        description: "All UK documents accepted Personal Papers Business Documents",
        country: 'philippines',
        image: '/static/images/svg/philippines.svg'
    },
    {
        title: "Thailand Embassy",
        subtitle:"5 Day Turnaround*",
        description: "All UK documents accepted Personal Papers Business Documents",
        country: 'thailand',
        image: '/static/images/svg/thailand.svg'
    },
    {
        title: "Saudi Arabia Embassy",
        subtitle:"5 Day Turnaround*",
        description: "All UK documents accepted Personal Papers Business Documents",
        country: 'saudi',
        image: '/static/images/svg/saudi.svg'
    },
    {
        title: "Malaysia Embassy",
        subtitle:"5 Day Turnaround*",
        description: "All UK documents accepted Personal Papers Business Documents",
        country: 'malaysia',
        image: '/static/images/svg/malaysia.svg'
    },
    {
        title: "Indonesia Embassy",
        subtitle:"5 Day Turnaround*",
        description: "All UK documents accepted Personal Papers Business Documents",
        country: 'indonesia',
        image: '/static/images/svg/indonesia.svg'
    },
    {
        title: "Iraq Embassy",
        subtitle:"5 Day Turnaround*",
        description: "All UK documents accepted Personal Papers Business Documents",
        country: 'iraq',
        image: '/static/images/svg/iraq.svg'
    },
];

var Vue = new Vue({
    el: "#app",
    vuetify: new Vuetify({
        theme: { disable: true }
    }),
    mixins: [MobileMixin],
    data: () => ({
        selectedE: 'uae',
        ems: dataEmbassys,
        state: 1,
        tab: null,
        items: [
            {id: 0, tab: 'Limited Company'},
            {id: 1, tab: 'Guarantee'},
            {id: 2, tab: 'LLP'},
        ],

    }),
    computed: {
        visibleEmbs() {
            if(!this.isMobile) return this.ems
            return this.ems.filter(a => a.country === this.selectedE)
        },
        countryFlag() {
            return (asd) => {
                return `/static/images/svg/${asd.country}.svg`
            }
        },
        textHeadline() {
            if ( this.state == 1 ) {
                return 'Choose where do you want to register your trademark:'
            } else if ( this.state == 2 ) {
                return 'Select the trade mark classification list of goods and services applicable to your products or services (scroll down to view all 45 classes)'
            } else if ( this.state == 3) {
                return 'Please provide some additional information'
            }
        },
    },
    methods: {
        setStep(value) {
            this.state = value
        },
        stepHandler(value) {
            this.setStep(value)
        },
        nextStep() {
            if (this.state === 3) return
            this.setStep(this.state + 1)
        }
    }
})



// Функция для открытия скрытых блоков FAQ
var $faqCircle = $('.circle');

$faqCircle.each( function(item) {
    var sub = this;
    var parent = $(this).closest('.faq__accordeon-block');

    $(sub).click( function(event) {
        event.preventDefault();

        $(this).toggleClass('open-content');
        $('.icon__triangle', parent).toggleClass('open-content');
        $('.faq__accordeon-body', parent).slideToggle('slow');
    })
});


// Функция для открытия скрытых блоков в регистрации
var $dropMenu = $('.reg-icon');

$dropMenu.each( function(item) {
    var sub = this;
    var parent = $(this).closest('.registration__accordeon-block');

    $(sub).click( function(event) {
        event.preventDefault();
        $(this).toggleClass('open-content');
        $('.registration__accordeon-body', parent).slideToggle('slow');
    })
});

// Функция для открытия бургер-меню
 var $burgerButton = $('.header__burger-btn');
 var $burgerContent = $('#header');
 var $burgerClose = $('.header__cross-btn');
 var $loginButton = $('#login_button');

$burgerButton.on('click', function() {
    $(this).addClass('open-burger');
    $('.body-main').addClass('overflow');
    // $burgerContent.slideToggle('slow');
    $burgerContent.addClass('burger-open');
    $loginButton.addClass('burger-open');
});
$burgerClose.on('click', function() {
    $burgerButton.removeClass('open-burger');
    $('.body-main').removeClass('overflow');
    // $burgerContent.slideToggle('slow');
    $burgerContent.removeClass('burger-open');
    $loginButton.removeClass('burger-open');
});


// Функция для открытия дополнительных списков в бургер-меню
var $listLink = $('.header-triangle');
$listLink.click( function(event) {
    event.preventDefault();
    const parent = $(this).closest('.header__nav-list');

    if($(window).width() <= 1024) {
        $(this).toggleClass('open-content');
        $('.sublist', parent).slideToggle('slow');
    } else {
        return false
    }
});

function  isMobile() {
    return $(window).width() < 1024
}

var $listItem = $('.sublist-link');
$listItem.each( function(item) {
    $(this).hover(
        function(event) {
            event.preventDefault();
            if (!isMobile()) {
                const parent = $(this).closest('.header__nav-list');
                $('.sublist', parent).addClass('open-content');
                $('.icon__triangle', parent).addClass('open-content');
            }
        },
        function(event) {
            event.preventDefault();
            if (!isMobile()) {
                const parent = $(this).closest('.header__nav-list');
                $('.sublist', parent).removeClass('open-content');
                $('.icon__triangle', parent).removeClass('open-content');
            }
        }
    )

});


// Функция для открытия второго выпадающего списка в мобильной версии
var $innerList = $('.icon__sublist-triangle');
$innerList.each( function(item) {
    var sub = this;
    var parent = $(this).closest('.header__nav-list');

    $(sub).click( function(event) {
        event.preventDefault();

        $('.icon__sublist-triangle', parent).toggleClass('open-content');
        $('.inner-sublist', parent).slideToggle('slow');
    })
});

// Функция для выравнивания высоты блоков в таблице
function equalHeight() {
    var $maxColHeight = 0;
    $('.equal-height').each( function() {
        if($(this).height() > $maxColHeight) {
            $maxColHeight = $(this).height();
        }
    });
    $('.equal-height').height($maxColHeight);
}
setTimeout(function(){
    equalHeight();
}, 500)

// Функция для выравнивания высоты ячеек в таблице
function equalRow() {
    var $maxColHeight = 0;
    $('.equal-row').each( function() {
        if($(this).height() > $maxColHeight) {
            $maxColHeight = $(this).height();
        }
    });
    $('.equal-row').height($maxColHeight);
}
setTimeout(function(){
    equalRow();
}, 500)



