var swipers = $('.swiper-container');

// for (let i = 0; i < swipers.length; i++) {
//     swipers.eq(i).attr('id', 'swiper-container' + i);
//
//     var nextBtn = $('#swiper-container' + i).find('.swiper-button-next');
//     var prevBtn = $('#swiper-container' + i).find('.swiper-button-prev');
//     var paginationBtn = $('#swiper-container' + i).find('.swiper-pagination');
//
//     // if (!!nextBtn.length) {
//     //     nextBtn.attr('id', 'swiper-button-next' + i)
//     // }
//     //
//     // if (!!prevBtn.length) {
//     //     prevBtn.attr('id', 'swiper-button-prev' + i)
//     // }
//
//     if (!!paginationBtn.length) {
//         paginationBtn.attr('id', 'swiper-pagination' + i)
//     }
//
//     let SwiperDefault = new Swiper ('#swiper-container' + i, {
//         direction: 'horizontal',
//         loop: false,
//         slidesPerView: 1,
//         // navigation: {
//         //     prevEl: '#swiper-button-prev' + i,
//         //     nextEl: '#swiper-slider-next' + i
//         // },
//         pagination: {
//             el: '.swiper-pagination',
//         },
//         breakpoints: {
//             //when window width is >=640px
//             640: {
//                 slidesPerView: 2,
//                 spaceBetween: 15
//             },
//             //when window width is >=1024px
//             1024: {
//                 slidesPerView: 3,
//                 spaceBetween: 20
//             },
//             //when window width is >=1200px
//             1200: {
//                 slidesPerView: 4,
//                 spaceBetween: 20
//             },
//             1366: {
//                 slidesPerView: 5,
//                 spaceBetween: 15,
//                 simulateTouch: false
//             }
//         }
//     });
//
//     if (!!prevBtn.length) {
//         prevBtn.click(function(){
//             let activeInd = SwiperDefault.activeIndex - 1;
//             SwiperDefault.slideTo(activeInd, 300);
//         })
//     }
//
//     if (!!nextBtn.length) {
//         nextBtn.click(function(){
//             let activeInd = SwiperDefault.activeIndex + 1;
//             SwiperDefault.slideTo(activeInd, 300);
//         })
//     }
// }

// Инициализация слайдера для списка пакетов для покупки
function packageSlider() {
    var packageSwiper = new Swiper('#package-swiper', {
        loop: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            1366: {
                slidesPerView: 5,
                spaceBetween: 15,
                simulateTouch: false
            },
            //when window width is >=1200px
            1200: {
                slidesPerView: 4,
                spaceBetween: 20
            },
            //when window width is >=1024px
            1024: {
                slidesPerView: 3,
                spaceBetween: 20
            },
            //when window width is >=640px
            640: {
                slidesPerView: 2,
                spaceBetween: 15
            }
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    // var nextBtn = $('.swiper-button-next');
    // var prevBtn = $('.swiper-button-prev');
    //
    // if (!!prevBtn.length) {
    //     prevBtn.click(function(){
    //         var activeInd = packageSwiper.activeIndex - 1;
    //         packageSwiper.slideTo(activeInd, 300);
    //     })
    // }
    //
    // if (!!nextBtn.length) {
    //     nextBtn.click(function(){
    //         var activeInd = packageSwiper.activeIndex + 1;
    //         packageSwiper.slideTo(activeInd, 300);
    //     })
    // }
};
packageSlider();

// Слайдер для списка отзывов
function reviewSlider() {
    var reviewSwiper = new Swiper('#review-swiper', {
        loop: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            // when window width is >=1200px
            1200: {
                slidesPerView: 4,
                spaceBetween: 20
            },
            // when window width is >=1024px
            1024: {
                slidesPerView: 3,
                spaceBetween: 20
            },
            // when window width is >=640px
            640: {
                slidesPerView: 2,
                spaceBetween: 15
            }
        }
    });
};
reviewSlider();

// Инициализация слайдера для списка парнеров
function partnerSlider() {
    var partnerSwiper = new Swiper('#partners-swiper', {
        loop: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            1280: {
                slidesPerView: 6,
                spaceBetween: 20,
                simulateTouch: false
            },
            //when window width is >=1024px
            1024: {
                slidesPerView: 5,
                spaceBetween: 20
            },
            //when window width is >=640px
            640: {
                slidesPerView: 2,
                spaceBetween: 15
            }
        }
    });

    // var nextBtn = $('.swiper-button-next');
    // var prevBtn = $('.swiper-button-prev');
    //
    // if (!!prevBtn.length) {
    //     prevBtn.click(function(){
    //         var activeInd = partnerSwiper.activeIndex - 1;
    //         partnerSwiper.slideTo(activeInd, 300);
    //     })
    // }
    //
    // if (!!nextBtn.length) {
    //     nextBtn.click(function(){
    //         var activeInd = partnerSwiper.activeIndex + 1;
    //         partnerSwiper.slideTo(activeInd, 300);
    //     })
    // }
};
partnerSlider();

// Инициализация слайдера для списка с сылками в футере
var footerSwiper = new Swiper('#footer-swiper', {
    loop: false,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    breakpoints: {
        //when window width is >=1366px
        1366: {
            slidesPerView: 5,
            spaceBetween: 15,
            simulateTouch: false
        },
        //when window width is >=1200px
        1200: {
            slidesPerView: 4,
            spaceBetween: 20
        },
        //when window width is >=1024px
        1024: {
            slidesPerView: 3,
            spaceBetween: 20
        },
        //when window width is >=640px
        640: {
            slidesPerView: 2,
            spaceBetween: 15
        }
    }
});

// Инициализаци слайдера для таблицы на странице с пакетами
function tableSlider() {
    var tableSwiper = new Swiper('#table-swiper', {
        loop: false,
        slidesPerView: 1,
        breakpoints: {
            //when window width is >=1024px
            1024: {
                slidesPerView: 5,
                simulateTouch: false
            },
            //when window width is >=320px
            320: {
                slidesPerView: 1,
            }
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    // var nextBtn = $('.swiper-button-next');
    // var prevBtn = $('.swiper-button-prev');
    //
    // prevBtn.click( function() {
    //     console.log('clickon the prev')
    // });
    // nextBtn.click( function() {
    //     console.log('clickon the next')
    // });
    //
    // if (!!prevBtn.length) {
    //     prevBtn.click(function(){
    //         let activeInd = tableSwiper.activeIndex - 1;
    //         tableSwiper.slideTo(activeInd, 300);
    //     })
    // };
    //
    // if (!!nextBtn.length) {
    //     nextBtn.click(function(){
    //         let activeInd = tableSwiper.activeIndex + 1;
    //         tableSwiper.slideTo(activeInd, 300);
    //     })
    // };
};
setTimeout(function(){
    tableSlider();
}, 0)





